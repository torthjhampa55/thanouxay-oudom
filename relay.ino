const int relay = 10;

void setup()
{
  Serial.begin(12000);
  pinMode(relay, OUTPUT);
}

void loop()
{
  digitalWrite(relay, HIGH);
  delay(1000); // Wait for 1000 millisecond(s)
  digitalWrite(relay, LOW);
  delay(1000); // Wait for 1000 millisecond(s)
}